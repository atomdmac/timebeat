-- CreateTable
CREATE TABLE "TimeEntry" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "creationId" TEXT NOT NULL,
    "time" BIGINT NOT NULL,
    "projectName" TEXT NOT NULL,
    "fileName" TEXT NOT NULL,
    "language" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL
);

-- CreateTable
CREATE TABLE "Tag" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_TagToTimeEntry" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    CONSTRAINT "_TagToTimeEntry_A_fkey" FOREIGN KEY ("A") REFERENCES "Tag" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_TagToTimeEntry_B_fkey" FOREIGN KEY ("B") REFERENCES "TimeEntry" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "TimeEntry_creationId_projectName_key" ON "TimeEntry"("creationId", "projectName");

-- CreateIndex
CREATE UNIQUE INDEX "_TagToTimeEntry_AB_unique" ON "_TagToTimeEntry"("A", "B");

-- CreateIndex
CREATE INDEX "_TagToTimeEntry_B_index" ON "_TagToTimeEntry"("B");
