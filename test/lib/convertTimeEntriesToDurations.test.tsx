import convertTimeEntriesToDurations from "../../app/lib/convertTimeEntriesToDurations";

describe("buckets", () => {
  it("should return buckets", () => {
    const timeEntries = [
      { time: 0, projectName: "a" },
      { time: 1000, projectName: "a" },
      { time: 1001, projectName: "a" },
      { time: 3000, projectName: "a" },
      { time: 4000, projectName: "a" },
      { time: 5000, projectName: "a" },
      { time: 10000, projectName: "a" },
    ];
    const output = convertTimeEntriesToDurations(timeEntries, 1000);
    expect(output.length).toEqual(2);
    expect(output[0].duration).toEqual(1001);
    expect(output[1].duration).toEqual(2000);
  });

  it("should return 0 buckets if no time entries are within max duration", () => {
    const timeEntries = [
      { time: 1000, projectName: "a" },
      { time: 2000, projectName: "a" },
      { time: 3000, projectName: "a" },
      { time: 4000, projectName: "a" },
    ];
    const output = convertTimeEntriesToDurations(timeEntries, 999);
    expect(output.length).toEqual(0);
  });

  it.skip("should not bucket entries together if they are not part of the same project.", () => {
    const timeEntries = [
      { time: 1000, projectName: "a" },
      { time: 2000, projectName: "b" },
      { time: 3000, projectName: "c" },
      { time: 4000, projectName: "d" },
    ];
    const output = convertTimeEntriesToDurations(timeEntries, 1000);
    expect(output.length).toEqual(0);
  });
});
