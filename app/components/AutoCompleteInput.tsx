import type { HTMLAttributes } from "react";
import { useEffect, useState } from "react";
import { useTypedFetcher } from "remix-typedjson";

export function defaultCreateListItem(value: any, selected: boolean) {
  return <option value={value.id}>{value.id}</option>;
}

export default function AutoCompeleteInput({
  renderListItem = defaultCreateListItem,
  ...rest
}: HTMLAttributes<HTMLInputElement> & {
  renderListItem: typeof defaultCreateListItem;
}) {
  const [value, setValue] = useState("");
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [focused, setFocused] = useState(false);
  const fetcher = useTypedFetcher<any>();

  useEffect(() => {
    if (fetcher.state === "idle" && value.length > 0) {
      fetcher.load(`/get-projects?projectName=${value}`);
    }
  }, [value]);

  const options =
    fetcher.data?.map((project: unknown, index: number) =>
      renderListItem(project, selectedIndex === index)
    ) ?? null;

  function increaseIndex() {
    setSelectedIndex(Math.min(selectedIndex + 1, options.length - 1));
  }

  function decreaseIndex() {
    setSelectedIndex(Math.max(selectedIndex - 1, 0));
  }

  function handleValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    setValue(event.target.value);
  }

  function handleFocus() {
    setFocused(true);
  }

  function handleBlur() {
    setFocused(false);
  }

  function handleKeyDown(event: React.KeyboardEvent<HTMLInputElement>) {
    console.log(event.key);
    if (event.key === "ArrowDown") {
      event.preventDefault();
      event.stopPropagation();
      increaseIndex();
    }
    if (event.key === "ArrowUp") {
      event.preventDefault();
      event.stopPropagation();
      decreaseIndex();
    }
  }

  function handleSelectItem(event: React.ChangeEvent<HTMLSelectElement>) {
    console.log("YOU SELECTED", event.target.value);
  }

  return (
    <div className="flex flex-col">
      <input
        onBlur={handleBlur}
        onChange={handleValueChange}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
        type="text"
        {...rest}
      />
      <div className="relative">
        <select
          onChange={handleSelectItem}
          className={`absolute w-full ${
            focused && options?.length ? "visible" : "visible"
          }`}
          multiple
        >
          {options}
        </select>
      </div>
    </div>
  );
}
