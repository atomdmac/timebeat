import type z from "zod";

export default async function getQueryParamsFromRequest<
  TSchema extends z.ZodSchema
>(request: Request, schema: TSchema): Promise<z.infer<TSchema>> {
  const url = new URL(request.url);
  const queryParams = Object.fromEntries(url.searchParams);
  return schema.parse(queryParams);
}
