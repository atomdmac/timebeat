export default function queryStringToObject(
  queryString: string
): Record<string, string> {
  return (
    queryString
      // Get rid of the leading `?`
      .slice(1)
      .split("&")
      .reduce((acc, curr) => {
        const [key, value] = curr.split("=");
        return { ...acc, [key]: value };
      }, {})
  );
}
