import { DateTime } from "luxon";
import type z from "zod";

// Converts an input specifying a time (ex."yesterday" or a unix timestamp) to
// a unix timestamp in seconds.
export function timeInputValueToSeconds(value: unknown) {
  if (typeof value === "number") {
    return BigInt(value);
  } else if (typeof value !== "string") {
    return BigInt(DateTime.now().toUnixInteger());
  } else {
    switch (value) {
      case "today":
        return BigInt(DateTime.now().startOf("day").toUnixInteger());
      case "yesterday":
        return BigInt(
          DateTime.now().minus({ days: 1 }).startOf("day").toUnixInteger()
        );
      case "last-7-days":
        return BigInt(
          DateTime.now().minus({ days: 7 }).startOf("day").toUnixInteger()
        );
      case "last-30-days":
        return BigInt(
          DateTime.now().minus({ days: 30 }).startOf("day").toUnixInteger()
        );
      default:
        return DateTime.now().toUnixInteger();
    }
  }
}
