import padNumber from "./padNumber";

export default function convertUnixTimeStampToDisplayTime(
  unixTimeStamp: bigint
): string {
  const date = new Date(Number(unixTimeStamp) * 1000);
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${year}-${padNumber(month)}-${padNumber(day)} ${padNumber(
    hours
  )}:${padNumber(minutes)}:${padNumber(seconds)}`;
}
