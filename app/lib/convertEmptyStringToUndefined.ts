// Converts an empty string to undefined. This is useful for form inputs that
// are optional but are coerced to an empty string when not provided.
export default function emptyStringToUndefined(value: string | undefined) {
  return value === "" ? undefined : value;
}
