import padNumber from "./padNumber";

export default function convertSecondsToComponents(
  unixTimeStamp: bigint
): string {
  const secondsPerHour = BigInt(3600);
  const minutesPerHour = BigInt(60);

  const hours = Number(unixTimeStamp / secondsPerHour);
  const minutes = Number((unixTimeStamp % secondsPerHour) / minutesPerHour);
  const seconds = Number((unixTimeStamp % secondsPerHour) % minutesPerHour);
  return `${padNumber(hours)}:${padNumber(minutes)}:${padNumber(seconds)}`;
}
