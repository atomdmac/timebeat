export type TimeEntryLike = {
  time: bigint;
  projectName: string;
};

export type Duration = {
  start: bigint;
  end: bigint;
  duration: bigint;
  projectName: string;
};

// TODO: Do not bucket items with disimilar tags/projects together.
export default function convertTimeEntriesToDurations(
  timeEntries: TimeEntryLike[],
  maxDurationMs: bigint
) {
  const { durations } = timeEntries.reduce(
    (acc, timeEntry, index, array) => {
      if (index === 0) return acc;

      const { time: currentTime } = timeEntry;

      // If the time between the current time entry and the last valid time entry
      // is less than or equal to the max duration, then we can consider the current
      // time entry part of the current duration.
      const durationSinceLastValidTimeEntry = currentTime - acc.lastValid.time;
      if (durationSinceLastValidTimeEntry <= maxDurationMs) {
        acc.lastValid = timeEntry;
      }

      // The "open" time entry is too far away from the next valid time entry which
      // means they cannot form a duration.
      if (acc.lastValid === acc.open) {
        acc.open = timeEntry;
        acc.lastValid = timeEntry;
        return acc;
      }

      if (
        durationSinceLastValidTimeEntry > maxDurationMs ||
        index === array.length - 1
      ) {
        acc.durations.push({
          duration: acc.lastValid.time - acc.open.time,
          projectName: acc.open.projectName,
          start: acc.open.time,
          end: acc.lastValid.time,
        });
        acc.open = timeEntry;
        acc.lastValid = timeEntry;
      }

      return acc;
    },
    {
      durations: [] as Duration[],
      open: timeEntries[0],
      lastValid: timeEntries[0],
    }
  );

  return durations;
}
