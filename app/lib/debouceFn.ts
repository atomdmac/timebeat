export default function debounceFn<T extends (...args: any[]) => any>(
  fn: T,
  wait: number
) {
  let timeout: NodeJS.Timeout | null = null;
  return function (...args: Parameters<T>) {
    const later = () => {
      timeout = null;
      fn(...args);
    };
    clearTimeout(timeout!);
    timeout = setTimeout(later, wait);
  };
}
