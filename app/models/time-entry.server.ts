import { prisma } from "~/db.server";

export async function createTimeEntry(data: {
  time: number;
  creationId: string;
  projectName: string;
  fileName: string;
  language: string;
}) {
  return await prisma.timeEntry.create({
    data,
  });
}

export async function searchProjectNames({
  projectName,
}: {
  projectName: string;
}) {
  return await prisma.timeEntry.findMany({
    distinct: ["projectName"],
    where: {
      projectName: {
        contains: projectName,
      },
    },
  });
}

export async function getTimeEntries({
  projectName,
  since,
  until,
}: {
  projectName?: string;
  since: bigint;
  until: bigint;
}) {
  return await prisma.timeEntry.findMany({
    where: {
      projectName,
      time: {
        gte: since,
        lte: until,
      },
    },
    include: {
      tags: true,
    },
    orderBy: [{ time: "asc" }, { projectName: "asc" }],
  });
}

export async function getTimeEntriesByProjectName({
  projectName = "",
  since,
  until,
}: {
  projectName?: string;
  since: bigint;
  until: bigint;
}) {
  const projectNames = await searchProjectNames({ projectName });
  return Promise.all(
    projectNames.map(async (project) => ({
      projectName: project.projectName,
      timeEntries: await getTimeEntries({
        projectName: project.projectName,
        since,
        until,
      }),
    }))
  );
}

export async function getAllTimeEntries() {
  return await prisma.timeEntry.findMany({
    include: {
      tags: true,
    },
    orderBy: [{ projectName: "asc" }, { time: "asc" }],
  });
}
