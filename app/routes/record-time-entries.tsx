import { json } from "@remix-run/node";
import { type ActionFunction } from "@remix-run/server-runtime";
import z from "zod";
import { createTimeEntry } from "~/models/time-entry.server";

const bodySchema = z.array(
  z.object({
    creationId: z.string(),
    time: z.coerce.number().int().positive(),
    projectName: z.string(),
    fileName: z.string().default("unknown"),
    language: z.string().default("unknown"),
  })
);

enum RecordTimeEntryStatus {
  Recorded = "recorded",
  Exists = "exists",
  Failed = "failed",
}

export const action: ActionFunction = async ({ request }) => {
  const parsedBody = await request.json();
  const body = bodySchema.parse(parsedBody);

  const results = await Promise.all(
    body.map(async (entry) => {
      try {
        await createTimeEntry(entry);
        return {
          creationId: entry.creationId,
          status: RecordTimeEntryStatus.Recorded,
        };
      } catch (error: any) {
        let status = RecordTimeEntryStatus.Failed;

        // Check if this error is due to a duplicate entry.
        // Reference: https://www.prisma.io/docs/reference/api-reference/error-reference#p2002
        if (error.code === "P2002") {
          status = RecordTimeEntryStatus.Exists;
        }

        return {
          creationId: entry.creationId,
          status,
        };
      }
    })
  );

  return json(results);
};
