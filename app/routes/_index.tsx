import { type LoaderArgs } from "@remix-run/node";
import { useLocation } from "@remix-run/react";
import { useEffect, type SelectHTMLAttributes } from "react";
import {
  typedjson,
  useTypedFetcher,
  useTypedLoaderData,
} from "remix-typedjson";
import z from "zod";
import emptyStringToUndefined from "~/lib/convertEmptyStringToUndefined";
import convertSecondsToComponents from "~/lib/convertSecondsToComponents";
import convertTimeEntriesToBuckets from "~/lib/convertTimeEntriesToDurations";
import { timeInputValueToSeconds } from "~/lib/convertTimeInputValueToSeconds";
import convertUnixTimeStampToDisplayTime from "~/lib/convertUnixTimeStampToDisplayTime";
import getQueryParamsFromRequest from "~/lib/getQueryParamsFromRequest";
import queryStringToObject from "~/lib/queryStringToObject";
import { getTimeEntriesByProjectName } from "~/models/time-entry.server";
import type { loader as getProjectsLoader } from "~/routes/get-projects";

const queryParamsSchema = z.object({
  timezone: z.string().optional().default("America/New_York"),
  maxDurationSec: z.coerce.bigint().default(BigInt(900)).pipe(z.bigint()),
  since: z.coerce
    .string()
    .optional()
    .default("today")
    .transform(timeInputValueToSeconds)
    .pipe(z.bigint()),
  until: z.coerce
    .string()
    .optional()
    .transform(timeInputValueToSeconds)
    .pipe(z.bigint()),
  projectName: z.coerce.string().transform(emptyStringToUndefined).optional(),
  fileName: z.coerce.string().transform(emptyStringToUndefined).optional(),
});

export const loader = async ({ request }: LoaderArgs) => {
  const data = await getQueryParamsFromRequest<typeof queryParamsSchema>(
    request,
    queryParamsSchema
  );
  const timeEntryGroups = await getTimeEntriesByProjectName(data);

  const output = timeEntryGroups.map(({ projectName, timeEntries }) => {
    const durations = convertTimeEntriesToBuckets(
      timeEntries,
      data.maxDurationSec
    );
    return {
      projectName: projectName,
      totalDuration: durations.reduce((totalSec, { duration }) => {
        return totalSec + duration;
      }, BigInt(0)),
      durations,
    };
  });

  return typedjson(output);
};

export function RelativeDateInput(
  props: SelectHTMLAttributes<HTMLSelectElement>
) {
  return (
    <select {...props}>
      <option value="today">
        Today ({timeInputValueToSeconds("today").toString()})
      </option>
      <option value="yesterday">
        Yesterday ({timeInputValueToSeconds("yesterday").toString()})
      </option>
      <option value="last-7-days">Last 7 Days</option>
      <option value="last-30-days">Last 30 Days</option>
    </select>
  );
}

export function ProjectNameInput(
  props: SelectHTMLAttributes<HTMLSelectElement>
) {
  const fetcher = useTypedFetcher<typeof getProjectsLoader>();

  useEffect(() => fetcher.load("/get-projects?projectName="), []);

  if (fetcher.state === "loading") return <div>Loading...</div>;

  return (
    <select {...props}>
      <option value="">All Projects</option>
      {fetcher.data?.map((p) => (
        <option key={p.id} value={p.projectName}>
          {p.projectName}
        </option>
      ))}
    </select>
  );
}

export function FilterForm() {
  const location = useLocation();
  const currentFormValues = queryStringToObject(location.search);

  return (
    <form method="get" className="my-4">
      <div className="-mx-3 flex flex-wrap items-end justify-between rounded bg-gray-100 p-3">
        <div className="mr-4 flex flex-col">
          <label htmlFor="projectName">Project Name</label>
          <ProjectNameInput
            id="projectName"
            name="projectName"
            defaultValue={currentFormValues.projectName}
          />
        </div>
        <div className="mr-4 flex flex-col">
          <label htmlFor="since">Since</label>
          <RelativeDateInput
            id="since"
            name="since"
            defaultValue={currentFormValues.since}
            className="w-[8em]"
          />
        </div>
        <div className="mr-4 flex flex-col">
          <label htmlFor="maxDurationSec">Max Duration</label>
          <input
            className="w-[8em]"
            id="maxDurationSec"
            name="maxDurationSec"
            type="number"
            defaultValue={currentFormValues.maxDurationSec || 900}
          />
        </div>
        <button className="primary" type="submit">
          Filter
        </button>
      </div>
    </form>
  );
}

export default function () {
  const loaderData = useTypedLoaderData<typeof loader>();
  return (
    <div>
      <h1>Durations</h1>
      <FilterForm />
      {loaderData.map((data) => (
        <div key={data.projectName} className="overflow-auto">
          <div className="mb-6 mt-8 border border-b border-gray-300" />
          <h2 className="my-4 capitalize">
            {data.projectName}
            <span className="ml-4 text-lg">
              {convertSecondsToComponents(data.totalDuration)}
            </span>
          </h2>
          <table className="w-full">
            <thead>
              <tr className="text-left">
                <th>Start</th>
                <th>End</th>
                <th>Duration (Formatted)</th>
                <th>Duration (Raw)</th>
              </tr>
            </thead>
            {data.durations.map((entry) => (
              <tr key={`${entry.start}_${entry.end}`}>
                <td className="pr-4">
                  {convertUnixTimeStampToDisplayTime(entry.start)}
                </td>
                <td className="pr-4">
                  {convertUnixTimeStampToDisplayTime(entry.end)}
                </td>
                <td className="pr-4">
                  {convertSecondsToComponents(entry.duration)}
                </td>
                <td className="pr-4">{entry.duration.toString()}</td>
              </tr>
            ))}
          </table>
        </div>
      ))}
      {!loaderData.length && "No durations found"}
    </div>
  );
}
