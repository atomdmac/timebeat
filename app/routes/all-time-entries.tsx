import { type V2_MetaFunction } from "@remix-run/node";
import { typedjson, useTypedLoaderData } from "remix-typedjson";

import { getAllTimeEntries } from "~/models/time-entry.server";

export const meta: V2_MetaFunction = () => [{ title: "Remix Notes" }];

export const loader = async () => {
  const data = await getAllTimeEntries();
  return typedjson(data);
};

export default function Index() {
  const loaderData = useTypedLoaderData<typeof loader>();

  if (!loaderData) return <div>loading...</div>;
  return (
    <div>
      <h1>Time Entries</h1>
      <ul>
        {loaderData.map((entry) => (
          <li key={entry.id}>{entry.projectName}</li>
        ))}
      </ul>
    </div>
  );
}
