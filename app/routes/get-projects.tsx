import { type LoaderArgs } from "@remix-run/node";
import { typedjson } from "remix-typedjson";
import z from "zod";
import getQueryParamsFromRequest from "~/lib/getQueryParamsFromRequest";
import { searchProjectNames } from "~/models/time-entry.server";

const querySchema = z.object({
  projectName: z.string().min(0).max(100),
});

export const loader = async (args: LoaderArgs) => {
  const data = await getQueryParamsFromRequest(args.request, querySchema);
  const results = await searchProjectNames(data);
  return typedjson(results);
};
